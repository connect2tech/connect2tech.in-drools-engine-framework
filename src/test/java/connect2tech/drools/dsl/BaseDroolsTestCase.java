package connect2tech.drools.dsl;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public abstract class BaseDroolsTestCase {
	private static KieContainer kContainer;
	protected KieSession knowledgeSession;
	private String sessionName;

	public BaseDroolsTestCase(String sessionName) {
		this.sessionName = sessionName;
		KieServices ks = KieServices.Factory.get();
		kContainer = ks.getKieClasspathContainer();
	}

	@BeforeSuite
	public void setUp() throws Exception {
		knowledgeSession = kContainer.newKieSession(sessionName);
	}

	@AfterSuite
	public void tearDown() throws Exception {
		knowledgeSession.dispose();
	}
}
