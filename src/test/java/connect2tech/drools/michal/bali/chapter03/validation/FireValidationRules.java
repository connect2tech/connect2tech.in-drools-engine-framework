package connect2tech.drools.michal.bali.chapter03.validation;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FireValidationRules {

	static KieContainer kieClasspathContainer;
	static StatelessKieSession ksession;

	@BeforeClass
	public static void before() {
		kieClasspathContainer = KnowledgeSessionHelper.createRuleBase();

	}

	@Test
	public void testAddressRequiredTest() {

		ksession = KnowledgeSessionHelper.getStatelessKieSession(kieClasspathContainer, "mbali.ch02.validation");
		ksession.addEventListener(new ValidationAgendaGroupEventListener());

		Customer customer = createCustomerBasic();
		ksession.execute(customer);
		ksession.execute(customer.getAccounts());
	}

	static Customer createCustomerBasic() {
		Customer customer = new Customer();
		Account account = new Account();
		customer.addAccount(account);
		account.setOwner(customer);
		return customer;
	}

}
