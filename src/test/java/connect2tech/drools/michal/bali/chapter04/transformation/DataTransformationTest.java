package connect2tech.drools.michal.bali.chapter04.transformation;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import connect2tech.drools.michal.bali.chapter03.validation.Country;
import connect2tech.drools.michal.bali.chapter03.validation.ValidationAgendaGroupEventListener;
import connect2tech.drools.michal.bali.utils.ErrorReportingHandlingUtil;

public class DataTransformationTest {

	static KieContainer kieClasspathContainer;
	static KieSession ksession;
	final static Logger logger = Logger.getLogger(DataTransformationTest.class);

	@BeforeSuite
	public void before_1() {

		try {
			ErrorReportingHandlingUtil.verifyDroolFileSyntactically(
					"connect2tech/drools/michal/bali/chapter04/transformation/etl-iBatis.drl", getClass());
			ksession = ErrorReportingHandlingUtil.verifyDroolFileNonSyntactically("mbali.ch04.transformation");
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@BeforeSuite
	public void before_2() {
		ksession.addEventListener(new ValidationAgendaGroupEventListener());
	}

	@Test(priority = 10)
	public void findCustomer() throws Exception {

		final Map customerMap = new HashMap();

		LegacyBankService service = new MockLegacyBankService() {
			@Override
			public List findAllCustomers() {
				return Arrays.asList(new Object[] { customerMap });
			}
		};

		ksession.setGlobal("legacyService", service);
		ksession.fireAllRules();

		Assert.assertEquals("Customer", customerMap.get("_type_"));

	}

	@Test(priority = 20, enabled = true)
	public void findAddress() throws Exception {
		final Map customerMap = new HashMap();
		customerMap.put("_type_", "Customer");
		customerMap.put("customer_id", new Long(111));

		final Map addressMap = new HashMap();
		LegacyBankService service = new MockLegacyBankService() {
			@Override
			public List findAddressByCustomerId(Long customerId) {
				Assert.assertEquals(customerMap.get("customer_id"), customerId);
				return Arrays.asList(new Object[] { addressMap });
			}
		};
		ksession.setGlobal("legacyService", service);
		ksession.insert(customerMap);
		ksession.fireAllRules();
		System.out.println("addressMap-->" + addressMap);

	}

	@Test(priority = 30, enabled = true)
	public void twoEqualAddressesDifferentInstance() throws Exception {
		Map addressMap1 = new HashMap();
		addressMap1.put("_type_", "Address");
		addressMap1.put("street", "Barrack Street");

		Map addressMap2 = new HashMap();
		addressMap2.put("_type_", "Address");
		addressMap2.put("street", "Barrack Street");

		// First asserting that the address are equal.
		Assert.assertEquals(addressMap1, addressMap2);

		ksession.insert(addressMap1);
		ksession.insert(addressMap2);
		ksession.fireAllRules();
	}

	@Test(priority = 40, enabled = true)
	public void addressNormalizationUSA() throws Exception {
		Map addressMap = new HashMap();
		addressMap.put("_type_", "Address");
		addressMap.put("country", "U.S.A");

		ksession.insert(addressMap);
		ksession.fireAllRules();

		Assert.assertEquals(Country.USA, addressMap.get("country"));
	}

	@Test(priority = 50, enabled = true)
	public void unknownCountry() throws Exception {
		Map addressMap = new HashMap();
		addressMap.put("_type_", "Address");
		addressMap.put("country", "no country");

		ksession.insert(addressMap);
		ksession.fireAllRules();
	}

	@Test(priority = 60, enabled = true)
	public void currencyConversionToEUR() throws Exception {
		Map accountMap = new HashMap();
		accountMap.put("_type_", "Account");
		accountMap.put("currency", "USD");
		accountMap.put("balance", "1000");

		ksession.insert(accountMap);
		ksession.fireAllRules();

		Assert.assertEquals("EUR", accountMap.get("currency"));
		Assert.assertEquals(new BigDecimal("780.000"), accountMap.get("balance"));
	}

	@Test(priority = 70, enabled = true)
	public void unknownCurrency() throws Exception {
		Map accountMap = new HashMap();
		accountMap.put("_type_", "Account");
		accountMap.put("currency", "unknown");
		accountMap.put("balance", "1000");

		ksession.insert(accountMap);
		ksession.fireAllRules();

		Assert.assertEquals("unknown", accountMap.get("currency"));
		Assert.assertEquals("1000", accountMap.get("balance"));
	}

	@Test(priority = 80, enabled = true)
	public void reduceLegacyAccounts() throws Exception {
		Map accountMap1 = new HashMap();
		accountMap1.put("_type_", "Account");
		accountMap1.put("customer_id", "00123");
		accountMap1.put("balance", new BigDecimal("100.00"));

		Map accountMap2 = new HashMap();
		accountMap2.put("_type_", "Account");
		accountMap2.put("customer_id", "00123");
		accountMap2.put("balance", new BigDecimal("300.00"));


		ksession.insert(accountMap1);
		ksession.insert(accountMap2);
		ksession.fireAllRules();
		
		Assert.assertEquals(new BigDecimal("400.00"), accountMap1.get("balance"));
	}

}
