package connect2tech.jboss.drools.license.main;

import java.io.IOException;

import org.drools.compiler.compiler.DroolsParserException;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import connect2tech.jboss.drools.license.Applicant;
import connect2tech.jboss.drools.license.Application;

public class LicenseApplicationTest {

	public static void main(String[] args) throws DroolsParserException, IOException {
		LicenseApplicationTest droolsTest = new LicenseApplicationTest();
		droolsTest.executeDrools();
	}

	public void executeDrools() throws DroolsParserException, IOException {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("License2");

		Applicant applicant = new Applicant();
		applicant.setAge(15);
		
		Application application = new Application();

		ksession.insert(applicant);
		ksession.insert(application);
		
		ksession.fireAllRules();

		System.out.println(applicant.isValid());
		System.out.println(application.isValid());

		ksession.dispose();

	}

}