package connect2tech.jboss.drools.firealarm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.drools.compiler.compiler.DroolsParserException;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class FireAlarmTest5 {

	public static void main(String[] args) throws DroolsParserException, IOException {
		FireAlarmTest5 droolsTest = new FireAlarmTest5();
		droolsTest.executeDrools();
	}

	public void executeDrools() throws DroolsParserException, IOException {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("Alarm1");

		String[] names = new String[] { "kitchen", "bedroom", "office", "livingroom" };

		for (String name : names) {

			Room room = new Room(name);
			ksession.insert(room);
			Sprinkler sprinkler = new Sprinkler(room);
			ksession.insert(sprinkler);
		}
		ksession.fireAllRules();

	}

}