package connect2tech.drools.michal.bali.utils;

import org.apache.log4j.Logger;

public class LoggerUtil {
	final static Logger logger = Logger.getLogger(LoggerUtil.class);
	
	public static void logInfo(String info){
		logger.info(info);
	}
}
