package connect2tech.drools.michal.bali.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.drools.verifier.Verifier;
import org.drools.verifier.builder.VerifierBuilder;
import org.drools.verifier.builder.VerifierBuilderFactory;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;

import connect2tech.drools.michal.bali.chapter03.validation.KnowledgeSessionHelper;
import connect2tech.drools.michal.bali.chapter03.validation.ValidationAgendaGroupEventListener;

public class ErrorReportingHandlingUtil {
	final static Logger logger = Logger.getLogger(ErrorReportingHandlingUtil.class);

	public static String getStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		return exceptionAsString;
	}

	private static String appendSpacesPrePost(String str) {

		StringBuffer sbuf = new StringBuffer();
		return sbuf.append(" ").append(str).append(" ").toString();
	}

	private static String appendColonPrePost(String str) {

		StringBuffer sbuf = new StringBuffer();
		return sbuf.append("::").append(str).append("::").toString();
	}

	private static String appendColonPrePost(int count) {

		StringBuffer sbuf = new StringBuffer();
		return sbuf.append("::").append(count).append("::").toString();
	}

	public static void verifyDroolFileSyntactically(String droolFile, Class<?> c) {

		VerifierBuilder vBuilder = VerifierBuilderFactory.newVerifierBuilder();
		Verifier verifier = vBuilder.newVerifier();
		verifier.addResourcesToVerify(ResourceFactory.newClassPathResource(droolFile, c), ResourceType.DRL);

		if (verifier.getErrors().size() == 0) {
			logger.debug(ErrorConstants.DROOL_FILE_NAME + appendColonPrePost(droolFile)
					+ ErrorConstants.DROOL_FILE_PASSED_SYNTACTICALLY);
		} else {
			logger.error(ErrorConstants.DROOL_FILE_NAME + appendColonPrePost(droolFile)
					+ ErrorConstants.DROOL_VERIFIER_ERROR_SIZE + appendColonPrePost(verifier.getErrors().size()));
			for (int i = 0; i < verifier.getErrors().size(); i++) {
				logger.error(ErrorConstants.DROOL_FILE_NAME + appendColonPrePost(droolFile)
						+ ErrorConstants.DROOL_VERIFIER_ERROR_MESSAGE
						+ appendColonPrePost(verifier.getErrors().get(i).getMessage()));
			}
			throw new RuntimeException("**************Drool File Syntactical Error**************");
		}

	}

	public static KieSession verifyDroolFileNonSyntactically(String packageName) {
		try {
			KieContainer kieClasspathContainer = KnowledgeSessionHelper.createRuleBase();
			KieSession ksession = KnowledgeSessionHelper.getStatefulKieSession(kieClasspathContainer, packageName);
			return ksession;

		} catch (Exception e) {
			logger.fatal("**************Drool File Non Syntactical Error**************", e);
			throw new RuntimeException(e.toString() + "**************Drool File Non Syntactical Error**************");
		}
	}
}
