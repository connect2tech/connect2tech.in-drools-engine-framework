package connect2tech.drools.michal.bali.utils;

public interface ErrorConstants {
	
	String DROOL_VERIFIER_ERROR_SIZE = "Drool Verifier Error Size";
	String DROOL_VERIFIER_ERROR_MESSAGE = "Drool Verifier Error Message";
	String DROOL_RULE_NAME = "Name of the Rule";
	String DROOL_FILE_NAME = "Name of the DRL file";
	String DROOL_FILE_PASSED_SYNTACTICALLY = "Passed Drool File Syntactically";

}
