package connect2tech.drools.michal.bali.chapter03.validation;

import java.io.PrintStream;

import org.apache.log4j.Logger;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.AgendaGroupPoppedEvent;
import org.kie.api.event.rule.AgendaGroupPushedEvent;
import org.kie.api.event.rule.BeforeMatchFiredEvent;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.MatchCancelledEvent;
import org.kie.api.event.rule.MatchCreatedEvent;
import org.kie.api.event.rule.RuleFlowGroupActivatedEvent;
import org.kie.api.event.rule.RuleFlowGroupDeactivatedEvent;

public class ValidationAgendaGroupEventListener extends DebugAgendaEventListener {

	final static Logger logger = Logger.getLogger(ValidationAgendaGroupEventListener.class);
	private PrintStream stream;

	public ValidationAgendaGroupEventListener() {
		super();
	}

	public ValidationAgendaGroupEventListener(PrintStream stream) {
		super(stream);
		this.stream = stream;
	}

	@Override
	public void matchCancelled(MatchCancelledEvent event) {
		// super.matchCancelled(event);
	}

	@Override
	public void matchCreated(MatchCreatedEvent event) {
		// super.matchCreated(event);
		logger.info("matchCreated::Rule Name="+event.getMatch().getRule().getName());
	}

	@Override
	public void afterMatchFired(AfterMatchFiredEvent event) {
		super.afterMatchFired(event);
		logger.info("afterMatchFired::Rule Name="+event.getMatch().getRule().getName());
		
	}

	@Override
	public void agendaGroupPopped(AgendaGroupPoppedEvent event) {
		// stream.println("==> " + event.getAgendaGroup() + " popped from stack
		// ");
	}

	@Override
	public void agendaGroupPushed(AgendaGroupPushedEvent event) {
		// stream.println("==> " + event.getAgendaGroup() + " pushed to stack
		// ");
	}

	@Override
	public void beforeMatchFired(BeforeMatchFiredEvent event) {
		// super.beforeMatchFired(event);
	}

	@Override
	public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
		// super.beforeRuleFlowGroupActivated(event);
	}

	@Override
	public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
		// super.afterRuleFlowGroupActivated(event);
	}

	@Override
	public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
		// super.beforeRuleFlowGroupDeactivated(event);
	}

	@Override
	public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
		// super.afterRuleFlowGroupDeactivated(event);
	}
}
