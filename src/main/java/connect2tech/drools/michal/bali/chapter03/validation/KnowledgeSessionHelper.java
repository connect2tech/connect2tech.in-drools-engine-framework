package connect2tech.drools.michal.bali.chapter03.validation;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;

public class KnowledgeSessionHelper {

	public static KieContainer createRuleBase() {
		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		return kieClasspathContainer;
	}

	public static StatelessKieSession getStatelessKieSession(KieContainer kieClasspathContainer, String sessionName) {
		// "mbali.ch02.validation"
		StatelessKieSession ksession = kieClasspathContainer.newStatelessKieSession(sessionName);
		return ksession;
	}
	
	public static KieSession getStatefulKieSession(KieContainer kieClasspathContainer, String sessionName) {
		// "mbali.ch02.validation"
		KieSession ksession = kieClasspathContainer.newKieSession(sessionName);
		return ksession;
	}

}
