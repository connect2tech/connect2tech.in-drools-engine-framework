package connect2tech.drools.michal.bali.chapter02;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class GlobalVariables {
	public static final void main(String[] args) {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("mbali.ch02.01");
		ksession.addEventListener(new AgendaGroupEventListener());

		List list = new ArrayList();
		ksession.setGlobal( "globalList", list );
		System.out.println("==== DROOLS SESSION START ==== ");
		ksession.fireAllRules();
		ksession.dispose();
		System.out.println("==== DROOLS SESSION END ==== ");

	}

}
