package connect2tech.drools.michal.bali.chapter02;

import java.util.ArrayList;
import java.util.List;

public class DataModel {

	List<String> list;
	
	String array[];
	
	int balance;
	String name;

	DataModel() {

		list = new ArrayList<String>();// Creating arraylist
		list.add("Ravi");// Adding object in arraylist
		list.add("Vijay");
		list.add("Ravi");
		list.add("Ajay");
		
		array = new String [] {"A","B"};

		balance = 200;

		name = "NC";

	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getArray() {
		return array;
	}

	public void setArray(String[] array) {
		this.array = array;
	}
	
	

}
