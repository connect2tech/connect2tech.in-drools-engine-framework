package connect2tech.drools.michal.bali.chapter02;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import connect2tech.drools.michal.bali.model.Account;

public class BasicRulesApp {
	public static final void main(String[] args) {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("mbali.ch02.01");
		ksession.addEventListener(new AgendaGroupEventListener());

		Account account = new Account();
		account.setBalance(50);
		ksession.insert(account);

		System.out.println("==== DROOLS SESSION START ==== ");
		ksession.fireAllRules();
		ksession.dispose();
		System.out.println("==== DROOLS SESSION END ==== ");

	}

}
