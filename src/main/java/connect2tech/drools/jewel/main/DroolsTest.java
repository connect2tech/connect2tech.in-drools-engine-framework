package connect2tech.drools.jewel.main;

import java.io.IOException;

import org.drools.compiler.compiler.DroolsParserException;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import connect2tech.drools.jewel.domain.Product;

public class DroolsTest {

	public static void main(String[] args) throws DroolsParserException, IOException {
		DroolsTest droolsTest = new DroolsTest();
		droolsTest.executeDrools();
	}

	public void executeDrools() throws DroolsParserException, IOException {

		KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kieClasspathContainer.newKieSession("Jewel1");

		Product product = new Product();
		product.setType("gold");

		ksession.insert(product);
		ksession.fireAllRules();

		System.out.println("The discount for the product " + product.getType() + " is " + product.getDiscount());

		ksession.dispose();

	}

}