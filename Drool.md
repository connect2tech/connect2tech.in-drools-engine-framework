# Framework 1.x
 * Ability to fire rules selectively
      * By default all the rules will get fired.
      * override_rule=OFF,Rule1,Rule2
 


 * Reporting / Test /Log4j (Rule Name, Details/Description)
 * "EQUALS Rule": How we can re-use
     * Identify all the rules where equality check is done for a given application/element.
     * Create a function which will check for equality for all the rules.
     * While the equality check is done, store the result in Wrapper Bean.
     * Store it in session. 
     * Perform the assertion in TestNG.
 * Externalize data
 * Custom Operations: Equals
 * Ease of writing drl file
   * Less java code in drl file
   * Create wrapper on Java
     * Parse the JSON, and then set the bean values. Custom Bean.
     * Convert JSON to Java and put wrapper on top of that.
       * Use reflection to generate bean with getter/setter by reading properties file
   
**Notes ToDo**
 
 * The fact does not changes.
 * Define detailed description of a rule and it should show up as part of report.
 
**What is done** 
 
 * Convert JSON to Java
 * 2 Rules
 
 
# Framework 2.x
 * Guvnor UI to create rules
 * DSL
 * Excel sheet / Workbook
 

# Where is my drools code

 * https://github.com/michalbali/droolsbook/blob/master/droolsBookParent/bankingcore/pom.xml
   * D:\naresh.chaurasia\Java-Architect\Drools\droolsbook
   * There were some errors in testcases, which were commented to do successful dependency:resolve eclipse:eclipse install

---

# Code Analysis

```
List<Passport> passports = ApplicationRepository.getPassports();
KieContainer kieContainer = KieServices.Factory.get().getKieClasspathContainer();
StatelessKieSession kieSession = kieContainer.newStatelessKieSession("StatelessPassportValidationStep" + step);
kieSession.execute(passports);


KieContainer kieClasspathContainer = KieServices.Factory.get().getKieClasspathContainer();
KieSession ksession = kieClasspathContainer.newKieSession("StatefulPassportValidationStep" + step);
List<Passport> passports = ApplicationRepository.getPassports();
passports.forEach(ksession::insert);
ksession.fireAllRules();
ksession.dispose();
```

# https://examples.javacodegeeks.com/enterprise-java/jboss-drools/jboss-drools-guvnor-example/

We use Guvnor as ‘Business Rules Manager’. Guvnor is the name of the web and network related components for managing rules with drools.
 
In order to know Drools Guvnor, we first need to know what is a business rules manager. Business Rules manager allows people to manage rules in a multi user environment. It is a single point of truth for your business rules. It allows to manage the rules in a controlled fashion. We can keep track of different versions of the rules, its deployment. It also allows multiple users of different skill levels access to edit rules with user friendly interfaces. Guvnor is the name of the web and network related components for managing rules with drools.

**Download Drools Guvnor:**
https://www.drools.org/download/download.html




# Drools JBoss Rules 5.X Developer's Guide: Michal Bali

**Code Setup**

```
$ pwd
/d/naresh.chaurasia/Java-Architect/droolsbook/droolsBookParent

794827@LTIN111597 MINGW64 /d/naresh.chaurasia/Java-Architect/droolsbook/droolsBookParent
$ mvn dependency:resolve install -Dmaven.test.skip=true

```


## Chapter1. Programming Declaratively

### Problems with traditional approaches


```
if (customer.getLevel() == Level.Gold) {
  // do something for Gold customer
} else if (customer.getLevel() == Level.Silver) {
  if (customer.getAccounts() == null) {
    // do something else for Silver customer with no accounts
  } else {
    for (Account account : customer.getAccounts()) {
      if (account.getBalance < 0) {
        // do something for Silver Customer that has 
        // account with negative balance
      }
    }
  }
}
```

 * The point of this Java "spaghetti code" is to give you an idea about what we are trying to prevent. 

 * You may think that it doesn't look that bad; however, after a couple of changes in requirements and developers who are maintaining the code, it can get much worse. It is usually the case that if you fix one bug, you are more likely to introduce several new bugs. 

 * Lots of requirements are literally packed into few lines of code. This code is hard to maintain or change to accommodate new requirements.

 * It is not only difficult to represent business logic in an imperative programming style language, but it is also hard to differentiate between code that represents the business logic and the infrastructure code that supports it. 
 
 * For developers, it is hard to change the business logic. For the domain experts, it is impossible to verify the business logic and even harder to change it.
 
**The Solution**

 * The problem is that with an imperative-style language, we are implementing both: what needs to be done (business requirements) and how it needs to be done (algorithm). 
 
 * Let's look at declarative-style programming, for example, the SQL in relational databases. The SQL describes what we want to search; it doesn't say anything about how the database should find the data. This is exactly what we need for our business requirements. 
 
 * A rule engine provides an alternative computation model. We declare rules in pretty much the same way as the business analyst does requirements, that is, as a group of if-then statements. 
 
 *The rule engine can then take these rules and execute them over our data in the most efficient way. Rules that have all their conditions true have their then part evaluated. This is different to imperative-style programming languages where the developer has to explicitly specify how it needs to be done (with sequence of conditionals and loops).

```
if Customer( level == Level.Gold )
then do something else for Gold customer

if Customer( level == Level.Silver )
and no Account( )
then do something for Silver customer who has no accounts

if Customer( level == Level.Silver)
and Account( balance < 0, customer.accounts contains account )
then do something for Silver Customer that has account with 
negative balance

```

**Easier to understand** 
Rules are easier to understand for a business analyst or a new developer than a program written in Java or other imperative-style language. It is more likely for a technically skilled business analyst to verify or change rules than a Java program. 

**Improved maintainability**: Since rules are easier to understand, a developer can spend more time solving the actual problem. 

**Dealing with evolving complexity**: It is much easier to add new, modify, or remove existing rules than to change, for example, a Java program. The impact this has on other rules is minimal in comparison with an imperative-style implementation. 

**Flexibility**: It deals better with changes to the requirements or changes to the data model. Changing or rewriting an application is never an easy task. However, thanks to the formalism that rules bring, it is much easier to change rules than to change a Java program. 

**Reasonable performance**: Thanks to the Rete algorithm that is behind Drools; in theory, the performance of the system doesn't depend on the number of rules. With every release of Drools, the performance of the engine is getting better by adding various optimizations such as Rete node sharing, node indexing, parallel execution, and so on. All this benefits new as well as old rules. 

**Translating requirements into rules**: The representation of business rules is consistent. For example, let's take some business rule and implement it in Java. Developers, depending on their experience, tend to use different ways to solve a problem. We'll find out that the possible solutions will vary greatly. Whereas with rules, this diversification becomes less apparent. It is simply because we are expressing "what" instead of "how". As a result the code is much easier to read even by new developers. 

**Ability to apply enterprise management to our rules**: This builds on the previous advantage of consistent representation. If we have consistent representation, it is much easier to introduce new features that apply across all our rules (for example, auditing, logging, reporting, or performance optimizations). 

**Reusability**: The rules are kept in one place (separation of business logic from the rest of the system), which means easier reusability. For example, imagine you've written some validation rules for your application and later on there is a need to do some batch imports of data, so you could simply reuse the validation rules in your batch import application. 

**Modeling the application closely**: Rules model the application invariants more closely. The imperative-style solutions tend to impose arbitrary and often unnecessary ordering on operations, depending on the algorithm chosen. This then hides the original invariants of the application. 

**Unifying**: The Drools platform brings unification of rules and processes. It is easy to call rules from a process or vice versa. 

**Redeploying**: It is possible to change/redeploy rules and processes without even stopping the whole application. 

---  

## Chapter2. Programming Declaratively

**Rule concepts** 
A rule can contain one or many conditions/patterns. For example: 

```
Account( balance == 200 )
Customer( name == "John" )
```

 Two rule conditions, one for type Account and one for type Customer Drools will then try to match every Account object in the session whose balance is equal to 200 with every Customer whose name is equal to John. If we have three Account objects that meet this criteria and two that don't, five Customer objects that meet this criteria and three that don't, it would create (3+2)*(5+3)=40 possible matches; however, only 3*5=15 of them valid. Meaning that a rule with these two conditions will be executed exactly 15 times.

**Variables in rules** 

```
$account : Account( $type : type ) 
```
This is a simple condition and matches every Account and creates two variables: $account and $type

```
$account : Account( )
Customer( account == $account )
```

These are conditions with a join and match every Customer with his/her Account

```
Customer( name == "John")
``` 

String matches every Customer with an equal name 

The following code matches an object with a name starting with an uppercase: 

```
Customer( name matches "@\[A-Za-z0-9\]*" )
``` 

Regular expression matching every Customer with a name that starts with an uppercase letter followed by one or many lowercase letters

```
Account(dateCreated> "01-Jan-2008" ) 
Transaction(isApproved == true )
Account( type == Account.Type.SAVINGS )
Account( balance == BigDecimal.ZERO )
Period(endDate.isAfter(startDate) ) 
```

---

## Chapter 5. The Rule Language

### 5.10. Domain Specific Languages

 * A set of DSL definitions consists of transformations from DSL "sentences" to DRL constructs, which lets you use of all the underlying rule language and engine features. Given a DSL, you write rules in DSL rule (or DSLR) files, which will be translated into DRL files.
 
 * DSLs can serve as a layer of separation between rule authoring (and rule authors) and the technical intricacies resulting from the modelling of domain object and the rule engine's native language and methods. If your rules need to be read and validated by domain experts (such as business analysts, for instance) who are not programmers, you should consider using a DSL; it hides implementation details and focuses on the rule logic proper. DSL sentences can also act as "templates" for conditional elements and consequence actions that are used repeatedly in your rules, possibly with minor variations. You may define DSL sentences as being mapped to these repeated phrases, with parameters providing a means for accomodating those variations.

 * DSLs have no impact on the rule engine at runtime, they are just a compile time feature, requiring a special parser and transformer.
 
```
[when]Something is {colour}=Something(colour=="{colour}")
``` 

 * In the preceding example, [when] indicates the scope of the expression, i.e., whether it is valid for the LHS or the RHS of a rule. The part after the bracketed keyword is the expression that you use in the rule; typically a natural language expression, but it doesn't have to be. The part to the right of the equal sign ("=") is the mapping of the expression into the rule language. The form of this string depends on its destination, RHS or LHS. If it is for the LHS, then it ought to be a term according to the regular LHS syntax; if it is for the RHS then it might be a Java statement.

---

https://docs.jboss.org/drools/release/5.3.0.Final/drools-expert-docs/html/ch01.html

**1.1. What is a Rule Engine?**

**1.1.1. Introduction and Background**

**1.2. Why use a Rule Engine?**

Some frequently asked questions:
1. When should you use a rule engine?
2. What advantage does a rule engine have over hand coded "if...then" approaches?
3. Why should you use a rule engine instead of a scripting framework, like BeanShell?

We will attempt to address these questions below.

**1.2.1. Advantages of a Rule Engine**

**Declarative Programming**

Rule engines allow you to say "What to do", not "How to do it".

The key advantage of this point is that using rules can make it easy to express solutions to difficult problems and consequently have those solutions verified. Rules are much easier to read than code.

Rule systems are capable of solving very, very hard problems, providing an explanation of how the solution was arrived at and why each "decision" along the way was made.

**Logic and Data Separation**

Your data is in your domain objects, the logic is in the rules. This is fundamentally breaking the OO coupling of data and logic, which can be an advantage or a disadvantage depending on your point of view. The upshot is that the logic can be much easier to maintain as there are changes in the future, as the logic is all laid out in rules. This can be especially true if the logic is cross-domain or multi-domain logic. Instead of the logic being spread across many domain objects or controllers, it can all be organized in one or more very distinct rules files.

**Speed and Scalability**

The Rete algorithm,the Leaps algorithm, and their descendants such as Drools' ReteOO, provide very efficient ways of matching rule patterns to your domain object data. These are especially efficient when you have datasets that change in small portions as the rule engine can remember past matches. These algorithms are battle proven.

**Centralization of Knowledge**

By using rules, you create a repository of knowledge (a knowledge base) which is executable. This means it's a single point of truth, for business policy, for instance. Ideally rules are so readable that they can also serve as documentation.

**Tool Integration**

Tools such as Eclipse (and in future, Web based user interfaces) provide ways to edit and manage rules and get immediate feedback, validation and content assistance. Auditing and debugging tools are also available.

**Explanation Facility**

Rule systems effectively provide an "explanation facility" by being able to log the decisions made by the rule engine along with why the decisions were made.

**Understandable Rules**

By creating object models and, optionally, Domain Specific Languages that model your problem domain you can set yourself up to write rules that are very close to natural language. They lend themselves to logic that is understandable to, possibly nontechnical, domain experts as they are expressed in their language, with all the program plumbing, the technical know-how being hidden away in the usual code.

**1.2.2. When should you use a Rule Engine?**

The shortest answer to this is "when there is no satisfactory traditional programming approach to solve the problem.". Given that short answer, some more explanation is required. The reason why there is no "traditional" approach is possibly one of the following:

 * The problem is just too fiddle for traditional code.
  
 The problem may not be complex, but you can't see a non-fragile way of building a solution for it.

 * The problem is beyond any obvious algorithmic solution.

 It is a complex problem to solve, there are no obvious traditional solutions, or basically the problem isn't fully understood.

 * The logic changes often

 The logic itself may even be simple but the rules change quite often. In many organizations software releases are few and far between and pluggable rules can help provide the "agility" that is needed and expected in a reasonably safe way.

 * Domain experts (or business analysts) are readily available, but are nontechnical.

 Domain experts often possess a wealth of knowledge about business rules and processes. They typically are nontechnical, but can be very logical. Rules can allow them to express the logic in their own terms. Of course, they still have to think critically and be capable of logical thinking. Many people in nontechnical positions do not have training in formal logic, so be careful and work with them, as by codifying business knowledge in rules, you will often expose holes in the way the business rules and processes are currently understood.

Typically in a modern OO application you would use a rule engine to contain key parts of your business logic, especially the really messy parts. This is an inversion of the OO concept of encapsulating all the logic inside your objects. This is not to say that you throw out OO practices, on the contrary in any real world application, business logic is just one part of the application. If you ever notice lots of conditional statements such as "if" and "switch", an overabundance of strategy patterns and other messy logic in your code that just doesn't feel right: that would be a place for rules. If there is some such logic and you keep coming back to fix it, either because you got it wrong, or the logic or your understanding changes: think about using rules. If you are faced with tough problems for which there are no algorithms or patterns: consider using rules.

Rules could be used embedded in your application or perhaps as a service. Often a rule engine works best as "stateful" component, being an integral part of an application. However, there have been successful cases of creating reusable rule services which are stateless.


1.2.3. When not to use a Rule Engine

1.2.4. Scripting or Process Engines

**1.2.5. Strong and Loose Coupling**

No doubt you have heard terms like "tight coupling" and "loose coupling" in systems design. Generally people assert that "loose" or "weak" coupling is preferable in design terms, due to the added flexibility it affords. Similarly, you can have "strongly coupled" and "weakly coupled" rules. Strongly coupled in this sense means that one rule "firing" will clearly result in another rule firing, and so on; in other words, there is a clear (probably obvious) chain of logic. If your rules are all strongly coupled, the chances are that the will turn out to be inflexible, and, more significantly, that a rule engine is an overkill. A clear chain can be hard coded, or implemented using a Decision Tree. This is not to say that strong coupling is inherently bad, but it is a point to keep in mind when considering a rule engine and the way you capture the rules. "Loosely" coupled rules should result in a system that allows rules to be changed, removed and added without requiring changes to other, unrelated rules.

---

**2.1. The Basics**

**2.1.1. Stateless Knowledge Session**

Stateless session, not utilising inference, forms the simplest use case. A stateless session can be called like a function passing it some data and then receiving some results back. Some common use cases for stateless sessions are, but not limited to:

**Validation**
 * Is this person eligible for a mortgage?

**Calculation**
 * Compute a mortgage premium.

**Routing and Filtering**
 * Filter incoming messages, such as emails, into folders.
 * Send incoming messages to a destination.

**2.1.2. Stateful Knowledge Session**

Stateful Sessions are longer lived and allow iterative changes over time. Some common use cases for Stateful Sessions are, but not limited to:

**Monitoring**
 * Stock market monitoring and analysis for semi-automatic buying.

**Diagnostics**
 * Fault finding, medical diagnostics

**Logistics**
 * Parcel tracking and delivery provisioning

**Compliance**
 * Validation of legality for market trades.

In contrast to a Stateless Session, the dispose() method must be called afterwards to ensure there are no memory leaks, as the Knowledge Base contains references to Stateful Knowledge Sessions when they are created. StatefulKnowledgeSession also supports the BatchExecutor interface, like StatelessKnowledgeSession, the only difference being that the FireAllRules command is not automatically called at the end for a Stateful Session.

Whereas the Stateless Session uses standard Java syntax to modify a field, in the above rule we use the modify statement, which acts as a sort of "with" statement. It may contain a series of comma separated Java expressions, i.e., calls to setters of the object selected by the modify statement's control expression. This modifies the data, and makes the engine aware of those changes so it can reason over them once more. This process is called **inference**, and it's essential for the working of a Stateful Session. Stateless Sessions typically do not use inference, so the engine does not need to be aware of changes to data. Inference can also be turned off explicitly by using the sequential mode.

**2.2. A Little Theory**

**2.2.1. Methods versus Rules**

```
public void helloWorld(Person person) {

    if ( person.getName().equals( "Chuck" ) ) {

        System.out.println( "Hello Chuck" );

    }

}
```

 * Methods are called directly.
 * Specific instances are passed.
 * One call results in a single execution.
 
```
rule "Hello World"
    when
        Person( name == "Chuck" )
    then
        System.out.println( "Hello Chuck" );
        end
```

 * Rules execute by matching against any data as long it is inserted into the engine.
 * Rules can never be called directly.
 * Specific instances cannot be passed to a rule.
 * Depending on the matches, a rule may fire once or several times, or not at all.

**2.2.2. Cross Products**

Earlier the term "cross product" was mentioned, which is the result of a join. Imagine for a moment that the data from the fire alarm example were used in combination with the following rule where there arr no field constraints:

```
rule
when
    $room : Room()
    $sprinkler : Sprinkler()
then
    System.out.println( "room:" + $room.getName() +
                        " sprinkler:" + $sprinkler.getRoom().getName() );
end
```

In SQL terms this would be like doing select * from Room, Sprinkler and every row in the Room table would be joined with every row in the Sprinkler table resulting in the following output:

```
room:office sprinker:office
room:office sprinkler:kitchen
room:office sprinkler:livingroom
room:office sprinkler:bedroom
room:kitchen sprinkler:office
room:kitchen sprinkler:kitchen
room:kitchen sprinkler:livingroom
room:kitchen sprinkler:bedroom
room:livingroom sprinkler:office
room:livingroom sprinkler:kitchen
room:livingroom sprinkler:livingroom
room:livingroom sprinkler:bedroom
room:bedroom sprinkler:office
room:bedroom sprinkler:kitchen
room:bedroom sprinkler:livingroom
room:bedroom sprinkler:bedroom
```

These cross products can obviously become huge, and they may very well contain spurious data. The size of cross products is often the source of performance problems for new rule authors. From this it can be seen that it's always desirable to constrain the cross products, which is done with the variable constraint.

```
rule
when
    $room : Room()
    $sprinkler : Sprinkler( room == $room )
then
    System.out.println( "room:" + $room.getName() +
                        " sprinkler:" + $sprinkler.getRoom().getName() );
end
```

This results in just four rows of data, with the correct Sprinkler for each Room. In SQL (actually HQL) the corresponding query would be select * from Room, Sprinkler where Room == Sprinkler.room.

```
room:office sprinkler:office
room:kitchen sprinkler:kitchen
room:livingroom sprinkler:livingroom
room:bedroom sprinkler:bedroom
```

2.2.3. Activations, Agenda and Conflict Sets.

2.2.4. Inference

2.2.5. Inference and TruthMaintenance

2.3. More on building and deploying

2.3.1. Knowledge Base by Configuration Using Changesets

2.3.2. Knowledge Agent

---

https://www.tutorialspoint.com/drools/index.htm

## What is Drools?
Drools is a **Business Logic integration Platform (BLiP)**. It is written in Java. It is an open source project that is backed by JBoss and Red Hat, Inc. It extends and implements the Rete Pattern matching algorithm.

In layman’s terms, Drools is a collection of tools that allow us to separate and reason over logic and data found within business processes. The two important keywords we need to notice are **Logic** and **Data**.

Drools is split into two main parts: Authoring and Runtime.

 * **Authoring** − Authoring process involves the creation of Rules files (.DRL files).

 * **Runtime** − It involves the creation of working memory and handling the activation.
 
### What is a Rule Engine?

Drools is Rule Engine or a Production Rule System that uses the rule-based approach to implement and Expert System. Expert Systems are knowledge-based systems that use knowledge representation to process acquired knowledge into a knowledge base that can be used for reasoning.

A Production Rule System is Turing complete with a focus on knowledge representation to express propositional and first-order logic in a concise, non-ambiguous and declarative manner.

The brain of a Production Rules System is an **Inference Engine** that can scale to a large number of rules and facts. The Inference Engine matches facts and data against Production Rules – also called **Productions** or just **Rules** – to infer conclusions which result in actions.

A Production Rule is a two-part structure that uses first-order logic for reasoning over knowledge representation. A business rule engine is a software system that executes one or more business rules in a runtime production environment.

A Rule Engine allows you to define **What to Do** and not **How to do it**.

### What is a Rule?
Rules are pieces of knowledge often expressed as, "When some conditions occur, then do some tasks."
```
When
   <Condition is true>
Then
   <Take desired Action>
```   
The most important part of a Rule is its when part. If the when part is satisfied, the then part is triggered.
```
rule  <rule_name>
   <attribute> <value>
      
   when
      <conditions>
      
   then
      <actions>
end
```

### Pattern Matching
The process of matching the new or existing facts against Production Rules is called Pattern Matching, which is performed by the Inference Engine. There are a number of algorithms used for Pattern Matching including −

 * Linear
 * Rete
 * Treat
 *  Leaps

Drools Implements and extends the Rete Algorithm. The Drools Rete implementation is called **ReteOO**, signifying that Drools has an enhanced and optimized implementation of the Rete algorithm for object-oriented systems.

### Advantages of a Rule Engine

**Declarative Programming**
Rules make it easy to express solutions to difficult problems and get the solutions verified as well. Unlike codes, Rules are written in less complex language; Business Analysts can easily read and verify a set of rules.

**Logic and Data Separation**
The data resides in the Domain Objects and the business logic resides in the Rules. Depending upon the kind of project, this kind of separation can be very advantageous.

**Speed and Scalability**
The Rete OO algorithm on which Drools is written is already a proven algorithm. With the help of Drools, your application becomes very scalable. If there are frequent change requests, one can add new rules without having to modify the existing rules.

**Centralization of Knowledge**
By using Rules, you create a repository of knowledge (a knowledge base) which is executable. It is a single point of truth for business policy. Ideally, Rules are so readable that they can also serve as documentation.

**Tool Integration**
Tools such as Eclipse provide ways to edit and manage rules and get immediate feedback, validation, and content assistance. Auditing and debugging tools are also available.

### Rules
The heart of the Rules Engine where you specify conditions (if ‘a’ then ‘b’).

### Facts
Facts are the data on which the rules will act upon. From Java perspective, Facts are the POJO (Plain Old Java Object).

### Session
A Knowledge Session in Drools is the core component to fire the rules. It is the knowledge session that holds all the rules and other resources. A Knowledge Session is created from the KnowledgeBase.

For the rules engine to work, Facts are inserted into the session and when a condition is met, the subsequent rule gets fired. A Session is of two types −

 * Stateless Knowledge Session
 * Stateful Knowledge Session

### Agenda
It’s a logical concept. The agenda is the logical place where activations are waiting to be fired.

### Activations
Activations are the then part of the rule. Activations are placed in the agenda where the appropriate rule is fired.

https://www.tutorialspoint.com/drools/drools_rules_writing.htm

### Conditions in Rules
A rule can contain many conditions and patterns such as −

 * Account (balance == 200)
 * Customer (name == "Vivek")
The above conditions check if the Account balance is 200 or the Customer name is “Vivek”.

### Variables in Rules
A variable name in Drools starts with a Dollar($) symbol.

 * $account − Account( )
 * $account is the variable for Account() class
Drools can work with all the native Java types and even Enum.

### Comments in Rules
The special characters, # or //, can be used to mark single-line comments.

For multi-line comments, use the following format:

```
/*
   Another line
   .........
   .........
*/
```

### Global Variables
Global variables are variables assigned to a session. They can be used for various reasons as follows −

 * For input parameters (for example, constant values that can be customized from session to session).
 * For output parameters (for example, reporting—a rule could write some message to a global report variable).
 * Entry points for services such as logging, which can be used within rules.
 
### Functions in Rules
Functions are a convenience feature. They can be used in conditions and consequences. Functions represent an alternative to the utility/helper classes. For example,

```
function double calculateSquare (double value) {
   return value * value;
}
```

### Dialect
A dialect specifies the syntax used in any code expression that is in a condition or in a consequence. It includes return values, evals, inline evals, predicates, salience expressions, consequences, and so on. The default value is Java. Drools currently supports one more dialect called MVEL. The default dialect can be specified at the package level as follows −

```
package org.mycompany.somePackage
dialect "mvel"
```

### MVEL Dialect
MVEL is an expression language for Java-based applications. It supports field and method/getter access. It is based on Java syntax.

### Salience
Salience is a very important feature of Rule Syntax. Salience is used by the conflict resolution strategy to decide which rule to fire first. By default, it is the main criterion.

We can use salience to define the order of firing rules. Salience has one attribute, which takes any expression that returns a number of type int (positive as well as negative numbers are valid). The higher the value, the more likely a rule will be picked up by the conflict resolution strategy to fire.

```
salience ($account.balance * 5)
```

The default salience value is 0. We should keep this in mind when assigning salience values to some rules only.

### Rule Consequence Keywords
Rule Consequence Keywords are the keywords used in the "then" part of the rule.

 * **Modify** − The attributes of the fact can be modified in the then part of the Rule.
 * **Insert** − Based on some condition, if true, one can insert a new fact into the current session of the Rule Engine.
 * **Retract** − If a particular condition is true in a Rule and you don’t want to act anything else on that fact, you can retract the particular fact from the Rule Engine.

Note − It is considered a very bad practice to have a conditional logic (if statements) within a rule consequence. Most of the times, a new rule should be created.